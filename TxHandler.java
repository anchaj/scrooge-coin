//package com.crypto.currency;

import java.util.ArrayList;
import java.util.List;

public class TxHandler {

	private UTXOPool pool;

	/**
	 * Creates a public ledger whose current UTXOPool (collection of unspent
	 * transaction outputs) is {@code utxoPool}. This should make a copy of
	 * utxoPool by using the UTXOPool(UTXOPool uPool) constructor.
	 */
	public TxHandler(UTXOPool utxoPool) {
		this.pool = new UTXOPool(utxoPool);
	}

	/**
	 * @return true if: (1) all outputs claimed by {@code tx} are in the current
	 *         UTXO pool, (2) the signatures on each input of {@code tx} are
	 *         valid, (3) no UTXO is claimed multiple times by {@code tx}, (4)
	 *         all of {@code tx}s output values are non-negative, and (5) the
	 *         sum of {@code tx}s input values is greater than or equal to the
	 *         sum of its output values; and false otherwise.
	 */
	public boolean isValidTx(Transaction tx) {
		List<UTXO> alreadyProcessedTransactions = new ArrayList<>();
		double sumOfInputValues = 0;
		double sumOfOutputValues = 0;
        
        if (tx == null)
        {
            return false;
        }

		for (int i = 0;  i < tx.numInputs(); i++) {
            Transaction.Input input = tx.getInput(i);
            if (input == null) {
                return false;
            }
            
			UTXO pendingTransaction = new UTXO(input.prevTxHash, input.outputIndex);
			// 1
			if (!pool.contains(pendingTransaction)) {
				return false;
			}

			//2
            Transaction.Output output = pool.getTxOutput(pendingTransaction);
            if (output == null) {
                return false;
            }
            
			if (!Crypto.verifySignature(output.address,
					tx.getRawDataToSign(i),
					input.signature)) {
				return false;
			}

			//3
			if (alreadyProcessedTransactions.contains(pendingTransaction)) {
				return false;
			}
			alreadyProcessedTransactions.add(pendingTransaction);


			//5
			sumOfInputValues += pool.getTxOutput(pendingTransaction).value;
		}

		//4 and 5
		for (int i = 0;  i < tx.numOutputs(); i++) {
            Transaction.Output output = tx.getOutput(i);
            if(output == null) {
                return false;
            }
            
            //4
			if (output.value < 0) { 
				return false;
			}
            
            // 5
			sumOfOutputValues += output.value;
		}

		//5
		if (sumOfInputValues < sumOfOutputValues) {
			return false;
		}

		return true;
	}


	/**
	 * Handles each epoch by receiving an unordered array of proposed
	 * transactions, checking each transaction for correctness, returning a
	 * mutually valid array of accepted transactions, and updating the current
	 * UTXO pool as appropriate.
	 */
	public Transaction[] handleTxs(Transaction[] possibleTxs) {
		List<Transaction> validTransactions = new ArrayList<>();
		for (Transaction transaction : possibleTxs) {
			if (isValidTx(transaction)) {
				validTransactions.add(transaction);
				for (Transaction.Input input: transaction.getInputs()) {
					pool.removeUTXO(new UTXO(input.prevTxHash, input.outputIndex));
				}

				int cnt = 0;
				for (Transaction.Output output: transaction.getOutputs()) {
					UTXO newCorrectTransaction = new UTXO(transaction.getHash(), cnt++);
					pool.addUTXO(newCorrectTransaction, output);
				}
			}
		}

		return null;
	}

}
